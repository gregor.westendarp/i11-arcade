# i11 Arcade

This repository holds the code, schematics and 3D models for a small arcade machine located in the i11 break room. This machine is based on the RP2040 microcontroller and uses a flat screen CRT display over a composite interface.

The machine:

![A small 3D-Printed arcade machine](./img/arcade.png)

____

Wiring up the electronics:

![Wiring diagram](./img/pinouts.png)

____

This project uses the the pi-pico-composite library by Alan Reed.

____

Games currently implemented:

- Pong

- Space Invaders

____

TODO:

- [X] Add images to README

- [ ] Add asteroids game

- [ ] Add pac-man game

- [X] Add space invaders game

- [X] Upload 3D models
