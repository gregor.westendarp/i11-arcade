#ifndef __INVADERS_H__
#define __INVADERS_H__

#include <stdint.h>

#define INVADERS_FRAME_INTERVAL_ms 10

typedef enum {
  INVADERS_DIRECTION_LEFT = -1,
  INVADERS_DIRECTION_STOP = 0,
  INVADERS_DIRECTION_RIGHT = 1,
} invaders_player_direction_t;

typedef struct {
    int x;
    int y;
} Coordinates;

void invaders_init(void);

void invaders_update(void);

void invaders_tick(void);

void draw_invaders(int x_pos, int y_pos);

void draw_player(void);

void gamerunning(void);

Coordinates get_invader_coordinates(int row, int col, int x_pos, int y_pos);

void invaders_move_player(uint32_t player_id, invaders_player_direction_t direction);

int calculate_alien_grid_sum(void);

int* check_invader_collision(int x, int y);

bool check_invader_touching_wall(int x);

void draw_missile(void);

void draw_acid(void);

#endif
