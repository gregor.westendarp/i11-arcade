#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include "invaders.h"
#include "vec2.h"
#include "renderer.h"
#include "connections.h"
#include "hardware/gpio.h"
#include "pico/stdlib.h"
#include "sound.h"

// Banner images
#include "i11.xbm"

// Load sprites
#include "bitmaps/alien1a.xbm"
#include "bitmaps/alien1b.xbm"
#include "bitmaps/alien2a.xbm"
#include "bitmaps/alien2b.xbm"
#include "bitmaps/ship.xbm"
#include "bitmaps/player.xbm"
#include "bitmaps/missile.xbm"

// Define playspace
#define ALIEN_VOFFSET 20
#define ALIEN_HOFFSET 60
#define ALIEN_SPACING 6 // Negative spacing between aliens
#define ALIEN_TICKSPEED 20 // Default is 20, Lower = faster
#define ALIEN_GRID_ROWS 5   // Number of rows in the grid
#define ALIEN_GRID_COLS 8  // Number of columns in the grid

// Define game state
#define STATE_TITLE 0
#define STATE_READY 1
#define STATE_RUNNING 2
#define STATE_OVER 3
#define STATE_WIN 4
int state = STATE_TITLE;

// Define levels
int alien_grid[ALIEN_GRID_ROWS][ALIEN_GRID_COLS] = {
    {1, 1, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 2, 2, 2, 1, 2},
    {2, 2, 2, 2, 2, 2, 2, 2},
    {0, 0, 2, 2, 2, 2, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}
};

int grid_level1[ALIEN_GRID_ROWS][ALIEN_GRID_COLS] = {
    {1, 1, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 2, 2, 2, 1, 2},
    {2, 2, 2, 2, 2, 2, 2, 2},
    {0, 0, 2, 2, 2, 2, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}
};

int grid_level2[ALIEN_GRID_ROWS][ALIEN_GRID_COLS] = {
    {1, 1, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 2, 2, 2, 1, 2},
    {2, 2, 0, 0, 0, 0, 2, 2},
    {2, 2, 2, 2, 2, 2, 2, 2},
    {1, 2, 1, 2, 2, 1, 2, 1}
};

int grid_level3[ALIEN_GRID_ROWS][ALIEN_GRID_COLS] = {
    {1, 1, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 2, 2, 2, 1, 2},
    {2, 2, 2, 2, 2, 2, 2, 2},
    {2, 2, 2, 2, 2, 2, 2, 2},
    {2, 2, 2, 2, 2, 2, 2, 2}
};

// Define structures and enums
typedef struct {
  uint32_t position;
  uint32_t score;
} player_t;

// Define variables for alien movement
int xoff_delta = 5;
int aliens_xoff = 0;
int aliens_yoff= 30;
clock_t lastvideoupdate = 0;

// Define audio stuff
int soundlength = 10;
bool playsound = 0;
clock_t lastsoundupdate = 0;

// Define variables for alien drawing
int drawnalien;
int drawnalienrow;
int rowoffset;

// Define player
int player_position;
int player_movement;
int player_shoot;

// Define missile
int missile_x;
int missile_y;
int *collision_info;

// Define acid
int acid_x;
int acid_y;
int alien_shoot = 0;

// Define lifes, score and level
long score;
int level = 1;
int lifes;

// Define functions
static void draw(void);
static void reset_game(void); 

// Define debug variables
char debug_xoff[3];
char debug_yoff[3];
char debug_shoot[3];

void invaders_tick(void) {
  // Parity function, do not use!
}

void invaders_init(void) {
  reset_game();
  renderer_init(draw);
  clock_t lastvideoupdate = clock();
}

void invaders_update(void) {
  renderer_run();
}

void invaders_move_player(uint32_t player_id, invaders_player_direction_t direction){
  if (player_id == 1) {
    player_movement = direction;
  }
  if (player_id == 2 && player_shoot != 1 && direction == 0) {
    player_shoot = 1;
    missile_x = (player_width / 2) + player_position;
    missile_y = 500;
    // Play shoot sound
    lastsoundupdate = clock();
    soundlength = 20;
    playsound = true;
    beep(BUZZER, 480, 7500);
  }
}

void draw(void) {
  // Runs each frame, call game logic stuff here!
  switch(state) {
  case STATE_RUNNING:
    gamerunning();
    break;
  case STATE_TITLE:
    beep(BUZZER, 100, 0); // Stop any audio
    playsound = false;
	  renderer_draw_image(0, 0, i11_width, i11_height, i11_bits);
    // Wait for coin insertion
    if (!gpio_get(COIN_INSERT)) {
      printf("[DEBUG] Coin inserted");
      score = 0;
      level = 1;
      lifes = 3;
      soundlength = 800;
      playsound = true;
      level = 1;
      for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
        for (int j = 0; j < ALIEN_GRID_COLS; j++) {
          alien_grid[i][j] = grid_level1[i][j];
        }
      }
      sleep_ms(100);
      state = STATE_READY;
    }
	  break;
  case STATE_READY:
    renderer_draw_string(380, 280, 3, "GET READY!", 10, JUSTIFY_CENTRE);
    if (playsound == false) {
      aliens_xoff = 0;
      aliens_yoff= 30;
      state = STATE_RUNNING;
    }
    break;
  case STATE_OVER:
    renderer_draw_string(380, 200, 3, "GAME OVER!", 10, JUSTIFY_CENTRE);
    renderer_draw_string(380, 260, 3, "SCORE:", 6, JUSTIFY_CENTRE);
    sprintf(debug_yoff, "%d", score);
    renderer_draw_string(380, 320, 3, debug_yoff, strlen(debug_yoff), JUSTIFY_CENTRE);
    renderer_draw_string(380, 400, 2, "PRESS FIRE BUTTON", 17, JUSTIFY_CENTRE);
    // Wait for button press to exit to title
    if (player_shoot == 1) {
      player_shoot == 0;
      state = STATE_TITLE;
    }
    break;
  case STATE_WIN:
    renderer_draw_string(380, 200, 3, "LEVEL CLEARED!", 14, JUSTIFY_CENTRE);
    renderer_draw_string(380, 260, 3, "SCORE:", 6, JUSTIFY_CENTRE);
    sprintf(debug_yoff, "%d", score);
    renderer_draw_string(380, 320, 3, debug_yoff, strlen(debug_yoff), JUSTIFY_CENTRE);
    if (playsound == false) {
      switch (level) {
      case 1:
        for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
          for (int j = 0; j < ALIEN_GRID_COLS; j++) {
            alien_grid[i][j] = grid_level1[i][j];
          }
        }
        break;
      case 2:
        for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
          for (int j = 0; j < ALIEN_GRID_COLS; j++) {
            alien_grid[i][j] = grid_level2[i][j];
          }
        }
        break;
      case 3:
        for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
          for (int j = 0; j < ALIEN_GRID_COLS; j++) {
            alien_grid[i][j] = grid_level3[i][j];
          }
        }
        break;
      default:
        level = 1;
        for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
          for (int j = 0; j < ALIEN_GRID_COLS; j++) {
            alien_grid[i][j] = grid_level1[i][j];
          }
        }
      }
      soundlength = 400;
      playsound = true;
      state = STATE_READY;
    }
    break;
  }

  // This code compares sound timers and stops playing audio if the time ended
  // To play audio, do the following:
  //lastsoundupdate = clock();
  //soundlength = <duration>;
  //playsound = true;
  //beep(BUZZER, <frequency>, <dutycycle>);
  if (playsound == true && clock() > lastsoundupdate + soundlength) {
    beep(BUZZER, 100, 0); // Stop any audio
    playsound = false;
  }
}

void gamerunning(void) {
  
  // Move invaders
  if (clock() > lastvideoupdate + ALIEN_TICKSPEED) {
    lastvideoupdate = clock();  // Update the lastvideoupdate
    if (check_invader_touching_wall(20) || check_invader_touching_wall(760)) {
      // Count in the other direction
      xoff_delta = -xoff_delta;
      // Lower Invaders by 20 pixels
      aliens_yoff = aliens_yoff + 20;
      // Play a beep
      lastsoundupdate = clock();
      soundlength = 10;
      playsound = true;
      beep(BUZZER, 230, 5000);
    }
    aliens_xoff += xoff_delta;
  }
  
  // If no acid is present, attempt to spawn acid
  if (!alien_shoot) {
    int non_zero_positions[ALIEN_GRID_ROWS * ALIEN_GRID_COLS][2];
    int count = 0;

    // Check which entries in the 2D array alien_grid are not 0.
    for (int i = 0; i < ALIEN_GRID_ROWS; i++) {
      for (int j = 0; j < ALIEN_GRID_COLS; j++) {
        if (alien_grid[i][j] != 0) {
          non_zero_positions[count][0] = i;
          non_zero_positions[count][1] = j;
          count++;
        }
      }
    }

    if (count > 0) {
      // Get a random non-0 field with rand()
      srand(time(NULL));
      int random_index = rand() % count;
      int random_row = non_zero_positions[random_index][0];
      int random_col = non_zero_positions[random_index][1];

      // Shoot acid projectile
      Coordinates coords = get_invader_coordinates(random_row, random_col, aliens_xoff + 6, aliens_yoff);
      acid_x = coords.x;
      acid_y = coords.y;
      alien_shoot = true;
    }
  }

  // Draw debugging information
  /*
  sprintf(debug_xoff, "%d", acid_x);
  sprintf(debug_yoff, "%d", acid_y);
  sprintf(debug_shoot, "%d", alien_shoot);
  renderer_draw_string(50, 40, 2, debug_xoff, strlen(debug_xoff), JUSTIFY_LEFT);
  renderer_draw_string(120, 40, 2, debug_yoff, strlen(debug_yoff), JUSTIFY_LEFT);
  renderer_draw_string(200, 40, 2, debug_shoot, strlen(debug_shoot), JUSTIFY_LEFT);
  */

  // Draw Score and Life (renaming later, im lazy so im just reusing the debug variables xD)
  sprintf(debug_yoff, "%d", score);
  sprintf(debug_shoot, "%d", lifes);
  renderer_draw_string(160, 40, 2, debug_yoff, strlen(debug_yoff), JUSTIFY_RIGHT);
  renderer_draw_string(230, 40, 2, debug_shoot, strlen(debug_shoot), JUSTIFY_LEFT);

  // Move player
  player_position = player_position + 4*player_movement; // Multiply specifies speed
  if (player_position <= 0) {
    player_position = 1;
  } else if (player_position >= 740) {
    player_position = 740;
  }

  // Check for collision between missile and alien
  if (player_shoot == 1) {
    int *collision_info = check_invader_collision(missile_x, missile_y);
    if (collision_info[0] == 1 && collision_info[3] != 0) {
      // Remove missile
      player_shoot = 0;
      // Remove the alien
      alien_grid[collision_info[1]][collision_info[2]] = 0;
      // Increase score
      switch (collision_info[3]) {
        case 1:
          score = score + 200;
          break;
        case 2:
          score = score + 100;
          break;
        default:
          score = score + 500;
      }
      // Play noise
      lastsoundupdate = clock();
      soundlength = 20;
      playsound = true;
      beep(BUZZER, 30, 2000);
    }
  }

  // Check collision between acid and player. 
  // Acid and missile use the same sprite so size is the same.
  if (acid_x + missile_width > player_position &&  // Check if the right side of the acid is beyond the left side of the player
      acid_x < player_position + player_width &&   // Check if the left side of the acid is before the right side of the player
      acid_y + missile_height > 500 &&        // Check if the bottom side of the acid is below the top side of the player
      acid_y < 500 + player_height) {         // Check if the top side of the acid is above the bottom side of the player
    alien_shoot = 0;
    lifes = lifes - 1; 
    // Play jingle
    beep(BUZZER, 30, 1000);
    sleep_ms(100);
    beep(BUZZER, 100, 0);
    player_position = 40;
  }

  // Check for Game Over or Win situation
  int *collision_info = check_invader_collision(player_position, 500);
  if (lifes < 1 || aliens_yoff > 490 || collision_info[0] == 1 && collision_info[3] != 0) {
    // Game Over jingle
    beep(BUZZER, 440, 5000);
    sleep_ms(100);
    beep(BUZZER, 380, 5000);
    sleep_ms(100);
    beep(BUZZER, 240, 5000);
    sleep_ms(100);
    beep(BUZZER, 100, 0);
    player_shoot == 0;
    state = STATE_OVER;
  } else if (calculate_alien_grid_sum() == 0) {
    // Level cleared jingle
    beep(BUZZER, 880, 5000);
    sleep_ms(200);
    beep(BUZZER, 988, 5000);
    sleep_ms(200);
    beep(BUZZER, 783, 5000);
    sleep_ms(200);
    beep(BUZZER, 392, 5000);
    sleep_ms(200);
    beep(BUZZER, 587, 5000);
    sleep_ms(300);
    beep(BUZZER, 100, 0);
    level++;
    score = score + 5000;
    soundlength = 600;
    playsound = true;
    state = STATE_WIN;
  }

  // --==### DRAWING GAME STUFF ###==--
  // Draw player
  draw_player();

  // Draw invaders
  draw_invaders(aliens_xoff, aliens_yoff);

  // Draw missile
  if (missile_y > 30) {
    missile_y = missile_y - 8;
    draw_missile();
  } else {
    player_shoot = false;
  }

  // Draw acid
  if (alien_shoot && acid_y < 520) {
    acid_y = acid_y + 4;
    draw_acid();
  } else {
    alien_shoot = false;
    acid_y = 0;
  }
}

static void reset_game(void) {
    player_shoot = 0;
}

Coordinates get_invader_coordinates(int row, int col, int x_pos, int y_pos) {
    Coordinates coords;
    coords.x = (col * (alien1a_width + alien1a_width - ALIEN_SPACING)) - 30 + ALIEN_HOFFSET + x_pos;
    coords.y = (row * (alien1a_height + 8)) + ALIEN_VOFFSET + y_pos;
    return coords;
}

int calculate_alien_grid_sum(void) {
    int sum = 0;

    for (int row = 0; row < ALIEN_GRID_ROWS; ++row) {
        for (int col = 0; col < ALIEN_GRID_COLS; ++col) {
            sum += alien_grid[row][col];
        }
    }

    return sum;
}

void draw_invaders(int x_pos, int y_pos) {
    for (int row = 0; row < ALIEN_GRID_ROWS; ++row) {
        for (int col = 0; col < ALIEN_GRID_COLS; ++col) {
            int alien_type = alien_grid[row][col];
            Coordinates coords = get_invader_coordinates(row, col, x_pos, y_pos);

            if (alien_type == 1) {
                if (x_pos % 2 == 0) {
                    renderer_draw_image(coords.x, coords.y, alien1a_width, alien1a_height, alien1a_bits);  // Animation frame 0 for alien1
                } else {
                    renderer_draw_image(coords.x, coords.y, alien1b_width, alien1b_height, alien1b_bits);  // Animation frame 1 for alien1
                }
            } else if (alien_type == 2) {
                if (x_pos % 2 == 0) {
                    renderer_draw_image(coords.x, coords.y, alien2a_width, alien2a_height, alien2a_bits);  // Animation frame 0 for alien2
                } else {
                    renderer_draw_image(coords.x, coords.y, alien2b_width, alien2b_height, alien2b_bits);  // Animation frame 1 for alien2
                }
            }
        }
    }
}

int* check_invader_collision(int x, int y) {
    static int result[4] = {0, 0, 0, 0};  // [hit, hit_row, hit_col, alien_type]

    for (int row = 0; row < ALIEN_GRID_ROWS; ++row) {
        for (int col = 0; col < ALIEN_GRID_COLS; ++col) {
            int type = alien_grid[row][col];
            if (type != 0) {
                Coordinates coords = get_invader_coordinates(row, col, aliens_xoff, aliens_yoff);

                int alien_width = (type == 1) ? alien1a_width : alien2a_width;
                int alien_height = (type == 1) ? alien1a_height : alien2a_height;

                // Check collision
                if (x >= coords.x && x < coords.x + alien_width && y >= coords.y && y < coords.y + alien_height) {
                    result[0] = 1;         // Collision detected
                    result[1] = row;       // Row of the invader
                    result[2] = col;       // Column of the invader
                    result[3] = type;      // Type of the invader (1 or 2)
                    return result;
                }
            }
        }
    }

    // No collision
    result[0] = 0;
    result[1] = 0;
    result[2] = 0;
    result[3] = 0;
    return result;
}

bool check_invader_touching_wall(int x) {
    for (int row = 0; row < ALIEN_GRID_ROWS; ++row) {
        for (int col = 0; col < ALIEN_GRID_COLS; ++col) {
            int type = alien_grid[row][col];
            if (type != 0) {
                Coordinates coords = get_invader_coordinates(row, col, aliens_xoff, aliens_yoff);

                int alien_width = (type == 1) ? alien1a_width : alien2a_width;
                int alien_height = (type == 1) ? alien1a_height : alien2a_height;

                // Check collision
                if (x >= coords.x && x < coords.x + alien_width) {
                    return true;
                }
            }
        }
    }
    // No collision
    return false;
}

void draw_player(void) {
  renderer_draw_image(player_position, 500, player_width, player_height, player_bits);
}

void draw_missile(void) {
  if (player_shoot == 1) {
    renderer_draw_image(missile_x, missile_y, missile_width, missile_height, missile_bits);
  }
}

void draw_acid(void) {
  renderer_draw_image(acid_x, acid_y, missile_width, missile_height, missile_bits);
}