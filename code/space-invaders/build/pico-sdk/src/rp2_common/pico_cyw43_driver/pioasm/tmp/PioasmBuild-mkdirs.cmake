# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/opt/pico-sdk/tools/pioasm"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pioasm"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/tmp"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/src/PioasmBuild-stamp"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/src"
  "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/src/PioasmBuild-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/src/PioasmBuild-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/westendarp/Documents/git/i11-arcade/spaceinvaders-test/build/pico-sdk/src/rp2_common/pico_cyw43_driver/pioasm/src/PioasmBuild-stamp${cfgdir}") # cfgdir has leading slash
endif()
