#ifndef __SOUND_H__
#define __SOUND_H__

#include "hardware/clocks.h"
#include "hardware/pwm.h"
#include "hardware/timer.h"
#include "hardware/gpio.h"

void beep(uint pin, uint freq, uint duty_c);

#endif
