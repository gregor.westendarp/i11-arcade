# How to build it:

Install the Pico SDK for C code to your machine. If you are using linux, it should be placed under `/opt/pico-sdk`.

Windows users will have to modify the `CMakeLists.txt` to include the PICO_SDK_PATH as a non-unix directory. PLEASE NOTE that early ARM builds of Windows 10 have issues linking the binary.

____

First step will be to make sure that you are in the `code` directory. If you are not, do `cd code` to enter it.

Now, enter the directory of the game you want to compile.

Clean up any old build files that might be left in your directory by running `rm -r build; mkdir build`.

Next step will be to enter the empty build directory using `cd build`.

Now, you can generate a Makefile by running `cmake ..`.

Finally, compile the program into a uf2 file by running `make`.
